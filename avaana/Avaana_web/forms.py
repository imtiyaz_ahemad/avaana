
from django import forms
from django.contrib.auth import (
    authenticate, get_user_model, password_validation,
)
from Avaana_web.models import *


class user_form(forms.ModelForm):
    """docstring for user_form"""
    first_name = forms.CharField(error_messages={'required': '*Please enter your First Name'})
    last_name = forms.CharField(error_messages={'required': '*Please enter your Last Name'})
    email=forms.EmailField(error_messages={'required':'*Please enter your Email ID'})
    password=forms.CharField(error_messages={'required':'*Please choose a Password'})
    phone_no=forms.CharField(required=False)
    avatar=forms.ImageField(required=False)

    class Meta:
        model = MyUser
        fields = ('email', 'password', 'first_name','last_name','phone_no','avatar')

    # def clean_email(self):
    #     email = self.cleaned_data["email"]
    #     try:
    #         user_email = MyUser.objects.get(email=email)
    #         raise forms.ValidationError("*Entered email address already exists.")
    #     except user_email.DoesNotExist:
    #         return email


    def save(self, commit=True):
        user = super(user_form, self).save(commit = False)
        password = self.cleaned_data["password"]
        user.set_password(password)
        first_name = self.cleaned_data['first_name']
        last_name = self.cleaned_data['last_name']
        avatar = self.cleaned_data['avatar']
        phone_no = self.cleaned_data['phone_no']
        if commit:
            user.save()
        return user
        

