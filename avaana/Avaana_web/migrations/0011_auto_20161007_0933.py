# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-10-07 09:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Avaana_web', '0010_appointment'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='businessservice',
            name='name_of_service',
        ),
        migrations.RemoveField(
            model_name='practitionerservice',
            name='name_of_service',
        ),
        migrations.AddField(
            model_name='businessservice',
            name='name',
            field=models.CharField(blank=True, max_length=30, verbose_name='name'),
        ),
        migrations.AddField(
            model_name='practitionerservice',
            name='name',
            field=models.CharField(blank=True, max_length=30, verbose_name='name'),
        ),
    ]
