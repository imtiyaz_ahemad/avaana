from django.db import models
import datetime
# Create your models here.
from django.conf import settings
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser

from django.utils.translation import ugettext_lazy as _

class Category(models.Model):
    name = models.CharField(max_length=30,blank=True)
    cat_type = models.CharField(max_length=30,blank=True)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)

    def __str__(self):
        return "%s " % self.name


class Location(models.Model):
    name = models.CharField(max_length=30,blank=True)
    loc_type = models.CharField(max_length=30,blank=True)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)

    def __str__(self):
        return "%s " % self.name        


class MyUserManager(BaseUserManager):
    def create_user(self, email, password=None):
        """
        Creates and saves a User with the given email, and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email)
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password,**extra): 
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            email,
            password=password
        )
        user.is_admin = True
        user.save(using=self._db)
        return user

class MyUser(AbstractBaseUser):
    """docstring for MyUser"""
    email = models.EmailField(('email address'),
        max_length=255,
        unique=True,
    )
    avatar = models.ImageField(null=True,blank=True) 
    first_name = models.CharField(('first name'),max_length=50,blank=False)
    last_name = models.CharField(('last name'),max_length=50,blank=False)
    phone_no = models.CharField(('phone number'),max_length=30,blank=True)
    uu_id = models.CharField(('user id'),max_length=50,blank=True)
    created_at = models.DateTimeField(('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(('updated_at'),auto_now = True,auto_now_add=False)

    objects = MyUserManager()
    USERNAME_FIELD = 'email'
    # REQUIRED_FIELDS = ['first_name','last_name']


class SocialAuth(models.Model):
    provider = models.CharField(max_length=30)
    u_id = models.CharField(max_length=191)
    user_id = models.ForeignKey(MyUser,on_delete=models.CASCADE)
    extra_data = models.CharField(max_length=30,blank=True,default='')
    last_login = models.DateTimeField(_('last_login'),auto_now = True,auto_now_add=False)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)

class Business(models.Model):
    name = models.CharField(_('name'),max_length=100,blank=True)
    abn = models.CharField(_('ABN'),max_length=30,unique=True)
    address = models.CharField(_('address'),max_length=255,blank=False)
    email = models.EmailField(_('email address'),max_length=255,unique=False)
    mobile_no = models.CharField(_('mobile number'),max_length=30,blank=True)
    tel_no = models.CharField(_('telephone number'),max_length=30,blank=True)
    website = models.CharField(_('website'),max_length=30,blank=True)
    logo_image = models.ImageField(null=True,blank=True)
    cancelation_policy = models.CharField(_('cancelation policy'),max_length=30,blank=True)
    max_lead_time = models.CharField(_('maximum lead time'),max_length=30,blank=True)
    max_num = models.CharField(_('max number'),max_length=30,blank=True)
    is_active = models.BooleanField(_('is_active'),default=False)
    hour_check = models.BooleanField(_('hour_check'),default=False)
    practitioner_check = models.BooleanField(_('practitioner_check'),default=False)
    payment_check = models.BooleanField(_('payent_check'),default=False)
    user = models.ForeignKey(MyUser,on_delete=models.CASCADE,related_name="user_business")
    mon_start_at = models.TimeField( default=datetime.time(0, 0))
    mon_end_at = models.TimeField( default=datetime.time(0, 0))
    tue_start_at = models.TimeField( default=datetime.time(0, 0))
    tue_end_at = models.TimeField( default=datetime.time(0, 0))
    wed_start_at = models.TimeField( default=datetime.time(0, 0))
    wed_end_at = models.TimeField( default=datetime.time(0, 0))
    thr_start_at = models.TimeField( default=datetime.time(0, 0))
    thr_end_at = models.TimeField( default=datetime.time(0, 0))
    fri_start_at = models.TimeField( default=datetime.time(0, 0))
    fri_end_at = models.TimeField( default=datetime.time(0, 0))
    sat_start_at = models.TimeField( default=datetime.time(0, 0))
    sat_end_at = models.TimeField( default=datetime.time(0, 0))
    sun_start_at = models.TimeField( default=datetime.time(0, 0))
    sun_end_at = models.TimeField( default=datetime.time(0, 0))
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)


class BusinessService(models.Model):
    business = models.ForeignKey(Business,on_delete=models.CASCADE,related_name="business_services")
    category = models.ForeignKey(Category,on_delete=models.CASCADE,related_name="category_b_service")
    name = models.CharField(_('name'),max_length=30,blank=True)
    length_of_service = models.IntegerField(null=True, blank=True)
    price = models.FloatField(_('price'),default=0, blank=True)
    discount_price = models.FloatField(_('discount price'),default=0, blank=True)
    booking_mode = models.BooleanField(_('booking mode'),default=False)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)

class BusinessHoliday(models.Model):
    note = models.CharField(_('note'),max_length=300)
    business = models.ForeignKey(Business,on_delete=models.CASCADE,related_name="business_holidays")
    holiday_date = models.DateField()
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)


class BusinessPractitioner(models.Model):
    business = models.ForeignKey(Business,on_delete=models.CASCADE,related_name="business_practitioner")
    name = models.CharField(_('name'),max_length=100)
    profile = models.CharField(_('profile'),max_length=100)
    username = models.CharField(_('username'),max_length=100)
    email = models.EmailField(_('email address'),max_length=255,unique=True,)
    password = models.CharField(_('password'),max_length=100)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)

class PractitionerService(models.Model):
    practitioner = models.ForeignKey(BusinessPractitioner,on_delete=models.CASCADE,related_name="practitoner_services")
    name = models.CharField(_('name'),max_length=30,blank=True)
    length_of_service = models.IntegerField(null=True, blank=True)
    price = models.FloatField(_('price'),default=0, blank=True)
    discount_price = models.FloatField(_('discount price'),default=0, blank=True)
    booking_mode = models.BooleanField(_('booking mode'),default=False)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)

class PractitionerHoilday(models.Model):
    note = models.CharField(_('note'),max_length=300,blank=True,null=True)
    practitioner = models.ForeignKey(BusinessPractitioner,on_delete=models.CASCADE,related_name="practitioner_holidays")
    holiday_date = models.DateField()
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)

class BusinessImages(models.Model):
    image = models.ImageField(null=True,blank=True)
    business = models.ForeignKey(Business,on_delete=models.CASCADE,related_name="business_images")
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)


class Appointment(models.Model):
    
    """ One Appointment should have relation with MyUser, Bussiness, Category , Location and BusinessService"""
    appoint_creator = models.IntegerField(default=0)
    business_user = models.ForeignKey(MyUser, on_delete=models.CASCADE , related_name="appointies")
    business = models.ForeignKey(Business,on_delete=models.CASCADE,related_name="appointment_for_business")
    business_service = models.ForeignKey(BusinessService,on_delete=models.CASCADE,related_name="business_service")
    business_category = models.ForeignKey(Category,on_delete=models.CASCADE,related_name="appointment_category")
    appointment_location = models.ForeignKey(Location,on_delete=models.CASCADE,related_name="appointment_location")
    business_practitioner = models.ForeignKey(BusinessPractitioner, on_delete=models.CASCADE , related_name="business_practitioner")
    appointment_ontime = models.DateTimeField()
    appointment_duration = models.DateTimeField()
    appointment_ondate = models.DateField()
    appointment_reference = models.CharField(_("about appointment"),max_length=300,blank=True)
    created_at = models.DateTimeField(_('created_at'),auto_now = False,auto_now_add=True)
    updated_at = models.DateTimeField(_('updated_at'),auto_now = True,auto_now_add=False)