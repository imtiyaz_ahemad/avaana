from rest_framework import serializers
from Avaana_web.models import *
from django.contrib.auth import get_user_model
from rest_framework.serializers import (
    HyperlinkedIdentityField,
    ModelSerializer,
    SerializerMethodField
    )


class Base64ImageField(serializers.ImageField):
    """
    A Django REST framework field for handling image-uploads through raw post data.
    It uses base64 for encoding and decoding the contents of the file.

    Heavily based on
    https://github.com/tomchristie/django-rest-framework/pull/1268

    Updated for Django REST framework 3.
    """

    def to_internal_value(self, data):
        from django.core.files.base import ContentFile
        import base64
        import six
        import uuid

        # Check if this is a base64 string
        if isinstance(data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid_image')

            # Generate file name:
            file_name = str(uuid.uuid4())[:12] # 12 characters are more than enough.
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension, )

            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):
        import imghdr

        extension = imghdr.what(file_name, decoded_file)
        extension = "png" if extension == "jpg" else extension

        return extension





##### SERIALIZER OF USER SERIALIZER #######
class UserSerializer(serializers.ModelSerializer):
	password = serializers.CharField(write_only=True,required=True,min_length=8,error_messages={"blank": "Password cannot be empty.","min_length": "Password too short.",},)
	# avatar = Base64ImageField(max_length=None, use_url=True,allow_null=True)
	# email = serializers.EmailField(required=True,error_messages={"blank": "Email cannot be empty.","required": "Email already exist."})
	class Meta:
		model = get_user_model()   
		fields = ('id','email','first_name','last_name','password', 'phone_no')
		# extra_kwargs = {'password': {'write_only': True},"email": {"error_messages": {"required": "Email ID already exist."}}}

	def create(self, validated_data):
		user = MyUser(
			email=validated_data['email'],
			first_name=validated_data['first_name'],  
			last_name=validated_data['last_name'],
		)
		user.set_password(validated_data['password'])
		user.save()
		return user

class BusinessSerializer(serializers.ModelSerializer):
    logo_image = Base64ImageField(max_length=None, use_url=True,allow_null=True)

    class Meta:
        model = Business

class BusinessServiceSerializer(serializers.ModelSerializer):
    service_type = SerializerMethodField()
    class Meta:
        model = BusinessService

    def get_service_type(self,obj):
        return "Popular"    


class BusinessHolidaySerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessHoliday      

class BusinessPractitionerSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessPractitioner

class PractitionerServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = PractitionerService

class PractitionerHolidaySerializer(serializers.ModelSerializer):
    class Meta:
        model = PractitionerHoilday      

class CategorySerializer(serializers.ModelSerializer):
    services = SerializerMethodField()

    def get_services(self,obj):
        return BusinessServiceSerializer(obj.category_b_service.all(),many=True).data

    class Meta:
        model = Category
        fields = ('id', 'name','cat_type','services')

class LocationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Location


class BusinessImagesSerializer(serializers.ModelSerializer):
    image = Base64ImageField(max_length=None, use_url=True,allow_null=True)

    class Meta:
        model = BusinessImages

class AppointmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appointment        