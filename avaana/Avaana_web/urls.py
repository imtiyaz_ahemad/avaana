from django.conf.urls import url
from django.contrib.auth import views as auth_views
from Avaana_web import views


app_name = 'Avaana_web'

urlpatterns = [
	# ******* WEBSITE URL ********
	url(r'^get/$', views.get_s),
	url(r'^$', views.landingpage),
	url(r'^a_login/$', views.login),
	url(r'^a_signup/$', views.signup),
	url(r'^a_place_order/$', views.place_order),
	url(r'^a_login_social_auth/$',views.login_social_auth),
	url(r'^a_createbusiness/$',views.createbusiness),
	url(r'^a_createbusinessservice/$',views.createbusinessservice),
	url(r'^a_createbusinessholiday/$',views.createbusinessholiday),
	url(r'^a_get_business/(?P<pk>[0-9]+)/$',views.get_business),
	url(r'^a_get_businessservice/(?P<pk>[0-9]+)/$',views.get_businessservice),
	url(r'^a_get_businessholiday/(?P<pk>[0-9]+)/$',views.get_businessholiday),
	url(r'^a_update_business/(?P<pk>[0-9]+)/$',views.update_business),
	url(r'^a_createpractitioner/$',views.createpractitioner),
	url(r'^a_create_business_images/$', views.create_business_images),
	url(r'^a_get_business_images/$', views.get_business_images),

	url(r'^a_get_user_business/(?P<pk>[0-9]+)/$',views.get_user_business),

	url(r'^a_get_all_category/$', views.get_all_category),
	url(r'^a_get_all_location/$', views.get_all_location),

	#search api
	url(r'^a_search_result/$', views.search_result),
	url(r'^a_appointment/$', views.appointment),



	]