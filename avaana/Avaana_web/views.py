
from django.db import IntegrityError

from Avaana_web.models import *
from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from django.contrib.auth import get_user_model
from Avaana_web.serializers import *
from rest_framework import generics
from django.views.decorators.csrf import csrf_exempt
import stripe
import datetime


# stripe.api_key = "sk_test_oKTNoVLgsSHwgkQ6zO9lho3R"
stripe.api_key = "sk_test_3HHXQDTRS9LHhm2GyPAyrUhI"
# cloudinary.uploader.upload("my_picture.jpg")



def landingpage(request):
    print('###---Landing Page View---###')
    return render(request, 'index.html')

@api_view(['GET','POST'])
def get_s(request):
    print("hello")
    return Response({"status":200,"Message":"Testing Api"})
    


@api_view(['POST'])
def signup(request):
    print('Sign Up API')
    print(request.data)
    params = request.data
    serializer = UserSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response({"status":200,"Message":"Signup Successfully.","User_Detail":serializer.data})
    else:
        print(serializer.errors)
        return Response({"status":500,"Message":serializer.errors,"Error":serializer.errors[list(serializer.errors)[0]][0]})


###--- Login Api ---###
@api_view(['POST'])
def login(request,format='json'):
    print("Request Data---")
    print(request.data)
    params = request.data
    email = request.data['email']
    password = request.data['password']
    try:
        user = get_user_model().objects.get(email=email.lower())
        serializer=UserSerializer(user)
        user_profile = serializer.data
        pass_valid = user.check_password(password) 
        if pass_valid:
            user.last_login=datetime.datetime.now()
            user.save()
            return Response({"status":200,"Message":"Login Successfully","user":user_profile})
        else:
            return Response({"status":500,"Message":"*Email & Password are not Correct"})
    except get_user_model().DoesNotExist:
        return Response({"status":500,"Message":"*Email & Password are not Correct"})



### Login Facebook Api
@api_view(['POST'])
def login_social_auth(request,format='json'):
    print('###---Social Login Auth---###')
    provider = ["facebook","google"]
    params = request.data
    print('params-----');
    print(params)
    print(params['provider'])
    # params['provider'] = request.data['provider']
    if 'provider' in params and (params['provider'] == "facebook" or params['provider'] == "google"):
        pass
    else:
        print('Provider not there')
        return Response({"status":500,"message":"Invalid provider name. %s" % provider})
    if 'u_id' in params and params['u_id'] is "":
        print('u_id not there')
        return Response({"status":500,"message":"Enter User_ID For Login."})	
    authentication = SocialAuth.objects.filter(provider=params['provider'],u_id=params['u_id']).first()
    chek_auth = get_user_model().objects.filter(email__iexact=params['email']).first()
    if authentication and chek_auth:
        print('BOTH ARE TRUE')
        return Response({"status":200,"Message":"You are successfully Logged in from %s." % params['provider'],"user_id":authentication.user_id.id})
    elif chek_auth:
        print('efrerewrew U_ID')
        auth = SocialAuth()
        auth.provider = params['provider']
        auth.u_id = params['u_id']
        auth.user_id = chek_auth
        auth.save()
        return Response({"status":200,"Message":"You are successfully Logged in from %s." % params['provider'],"user_id":auth.user_id.id})
    else:
        print('registration not there')
        return Response({"status":200,"Message": "Please register first","user_id":"blank"})


@api_view(['POST'])
def place_order(request,format='json'):
    print('###---Place order API---###')
    params = request.data
    print('params-----');
    print(params)
    try:
        data=stripe.Charge.create(
            amount=params['amount'],
            currency=params['currency'],
            source={
                "number": params['card_number'],
                "exp_month": params['exp_month'],
                "exp_year": params['exp_year'],
                "cvc": params['cvc']
            },
            description="Charge for charlotte.brown@example.com"
        )
    except Exception as e:
        return Response({"status":500,"Message": "Payment Unsuccessfull"})
    return Response({"status":200,"Message": "Payment Successfull"})

    

@api_view(['GET','POST'])
def createbusiness(request,format='json'):
    print(request.data)
    params = request.data
    serializer = BusinessSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response({"status":200,"Message":"Business Created Successfully.","Business_Detail":serializer.data})
    else:
        print(serializer.errors)
        return Response({"status":500,"Message":serializer.errors,"Error":serializer.errors[list(serializer.errors)[0]][0]})
    return Response({"status":200,"Message":"Business Created Successfully."})

@api_view(['GET','POST'])
def createbusinessservice(request,format='json'):
    print(request.data)
    params = request.data
    serializer = BusinessServiceSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response({"status":200,"Message":"Business service Created Successfully.","Business_Detail":serializer.data})
    else:
        print(serializer.errors)
        return Response({"status":500,"Message":serializer.errors,"Error":serializer.errors[list(serializer.errors)[0]][0]})
    return Response({"status":200,"Message":"Business Service Created Successfully."})

	
@api_view(['GET','POST'])
def createbusinessholiday(request,format='json'):
    print(request.data)
    params = request.data
    serializer = BusinessHolidaySerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response({"status":200,"Message":"Business Hoilday Created Successfully.","Business_Detail":serializer.data})
    else:
        print(serializer.errors)
        return Response({"status":500,"Message":serializer.errors,"Error":serializer.errors[list(serializer.errors)[0]][0]})
    return Response({"status":200,"Message":"Business Holiday Created Successfully."})

@api_view(['GET','POST'])
def get_business(request,pk=None,format='json'):
    print(request.data)
    params = request.data
    try:
        business = Business.objects.get(pk=pk)
    except Business.DoesNotExist:
            return Response({"status":500,"Message":"Business id Is not correct"})    
    serializer = BusinessSerializer(business)
    services = BusinessServiceSerializer(business.business_services.all(),many=True).data
    holidays = BusinessHolidaySerializer(business.business_holidays.all(),many=True).data
    practitioners = BusinessPractitionerSerializer(business.business_practitioner.all(),many = True).data
    return Response({"status":200,"Message":"Business Detail Profile","Business":serializer.data,"BusinessService":services,"BusinessHoliday":holidays,"BusinessPractitioner":practitioners })

@api_view(['GET','POST'])
def get_businessservice(request,pk=None,format='json'):
    print(request.data)
    params = request.data
    try:
        business = BusinessService.objects.get(user_id=pk)
        if request.method == "GET":
            serializer = BusinessSerializer(business)
            return Response({"status":200,"Message":"Business Detail Profile","Business":serializer.data})
        elif request.method == "POST":
            serializer = BusinessSerializer(business,data = params )
            if serializer.is_valid():
                print(serializer.validated_data)
                serializer.save()
                return Response({"status":200,"Message":"Business Detail Profile","Business":serializer.data})
            else:
                print(serializer.errors)
                return Response({"status":500,"Message":"Image Upload Error"})    

    except BusinessService.DoesNotExist:
            return Response({"status":500,"Message":"Business id Is not correct"})    
    

@api_view(['GET','POST'])
def get_businessholiday(request,pk=None,format='json'):
    print(request.data)
    params = request.data
    try:
        business = BusinessHoliday.objects.get(business_id=pk)
    except BusinessHoliday.DoesNotExist:
            return Response({"status":500,"Message":"Business id Is not correct"})    
    serializer = BusinessSerializer(business)
    return Response({"status":200,"Message":"Business Detail Profile","Business":serializer.data})

@api_view(['GET','POST'])
def update_business(request,pk=None,format='json'):
    print(request.data)
    params = request.data
    services = params["serviceTime"]
    service_added = []
    holidays = params["holidays"]
    holidays_added = []
    try:
        business = Business.objects.get(pk=pk)
        if request.method == "GET":
            serializer = BusinessSerializer(business)
            return Response({"status":200,"Message":"Business Detail Profile","Business":serializer.data})
        elif request.method == "POST":
            serializer = BusinessSerializer(business,data = params)
            if serializer.is_valid():
                print(serializer.validated_data)
                serializer.save()
                for i in range(0,len(holidays)):
                    holi_hash = {}
                    holi_hash["holiday_date"] = holidays[i]
                    holi_hash["note"] = "Note"
                    holi_hash["business"] = serializer.data["id"]
                    serializer_hoilday = BusinessHolidaySerializer(data=holi_hash)
                    if serializer_hoilday.is_valid():
                        serializer_hoilday.save()
                        holidays_added.append(serializer_hoilday.data)
                        print("Holiday Save")
                    else:
                        print("Not save")
                        print(serializer_hoilday.errors)
                for i in range(0,len(services)):
                    print(services[i])
                    services[i]["name"] = services[i]["service"]
                    services[i]["business"] = serializer.data["id"]
                    serilaizer_services = BusinessServiceSerializer(data=services[i])
                    if serilaizer_services.is_valid():
                        serilaizer_services.save()
                        holidays_added.append(serilaizer_services.data)
                        print("services Save")
                    else:
                        print("Not save")
                        print(serilaizer_services.errors)




                return Response({"status":200,"Message":"Business Detail Profile","Business":serializer.data})
            else:
                print(serializer.errors)
                return Response({"status":500,"Message":"Error","Error_list":serializer.errors})
    except BusinessService.DoesNotExist:
            return Response({"status":500,"Message":"Business id Is not correct"})



@api_view(['GET','POST'])
def createpractitioner(request,format='json'):
    print(request.data)
    params = request.data
    services = params["serviceTime"]
    service_added = []
    holidays = params["holidays"]
    holidays_added = []
    serializer = BusinessPractitionerSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        for i in range(0,len(holidays)):
            holi_hash = {}
            holi_hash["holiday_date"] = holidays[i]
            holi_hash["note"] = "Note"
            holi_hash["practitioner"] = serializer.data["id"]
            serializer_hoilday = PractitionerHolidaySerializer(data=holi_hash)
            if serializer_hoilday.is_valid():
                serializer_hoilday.save()
                holidays_added.append(serializer_hoilday.data)
                print("Holiday Save")
            else:
                print("Not save")
                print(serializer_hoilday.errors)
        for i in range(0,len(services)):
            print(services[i])
            services[i]["name"] = services[i]["service"]
            services[i]["practitioner"] = serializer.data["id"]
            serilaizer_services = PractitionerServiceSerializer(data=services[i])
            if serilaizer_services.is_valid():
                serilaizer_services.save()
                holidays_added.append(serilaizer_services.data)
                print("services Save")
            else:
                print("Not save")
                print(serilaizer_services.errors)           

        return Response({"status":200,"Message":"Business Practitioner Created Successfully.","Detail":serializer.data,"holidays_added": holidays_added})
    else:
        print(serializer.errors)
        return Response({"status":500,"Message":serializer.errors,"Error":serializer.errors[list(serializer.errors)[0]][0]})
    return Response({"status":200,"Message":"Business Practitioner Created Successfully."})


@api_view(['GET'])
def get_user_business(request,pk=None,format='json'):
    try:
        user = MyUser.objects.get(pk=pk)
        data = {}
        data["business_id"] = False
        data["hour_check"] = False
        data["practitioner_check"] = False
        user_business = user.user_business.first()
        if user_business:
            data["business_id"] = user_business.id
            data["hour_check"] = user_business.hour_check
            data["practitioner_check"] = user_business.practitioner_check
            data["payment_check"] = user_business.payment_check
            return Response({"status":200,"Message":"Business Check Fetch Successfully.","check_detail":data})
        else:
            return Response({"status":200,"Message":"no BUSINESS","check_detail":data})
    except MyUser.DoesNotExist:
        return Response({"status":500,"Message":"User ID Is not correct"})


@api_view(['GET'])
def get_all_category(request):
    all_cat = Category.objects.all()
    serializer = CategorySerializer(all_cat, many=True)
    return Response({"status":200,"Message":"All category","categories":serializer.data})

@api_view(['GET'])
def get_all_location(request):
    all_loc = Location.objects.all()
    serializer = LocationSerializer(all_loc, many=True)
    return Response({"status":200,"Message":"All Locations","locations":serializer.data})

@api_view(['GET','POST','OPTIONS'])
def search_result(request,format='json'):
    print(request.method)
    params = request.data
    more_for_money = []
    for a in range(0,3):
        data = {}
        data["popular_result"] = "result1"
        data["msg"] ="msg"
        data["content"] = "content"
        data["imageSrc"] = "http://www.avaana.com.au/earlybird/images/avaana-logo-hero.png"  
        more_for_money.append(data)
    main_result = []
    for a in range(0,3):
        data = {}
        data["popular_result"] = "result1"
        data["msg"] ="msg"
        data["content"] = "content"
        data["imageSrc"] = "http://www.avaana.com.au/earlybird/images/avaana-logo-hero.png"
        data["lat"] = 28.7041
        data["lng"] = 77.1025
        data["cost"] = 55
        data["rating"] = 3
        data["score"] = 40
        data["distance"] = 3.5
        main_result.append(data)

    return Response({"status":200,"Message":"Search Result","main_result":main_result,"more_for_money":more_for_money})

@api_view(['POST','OPTIONS'])
def create_business_images(request,format='json'):
    serializer = BusinessImagesSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response({"status":200,"Message":"Business Image Uploaded Successfully.","Business_Image":serializer.data})
    else:
        print(serializer.errors)
        return Response({"status":500,"Message":serializer.errors,"Error":serializer.errors[list(serializer.errors)[0]][0]})


@api_view(['GET','POST'])
def get_business_images(request,pk=None,format='json'):
    print(request.data)
    params = request.data
    try:
        business = Business.objects.get(pk=pk)
    except Business.DoesNotExist:
            return Response({"status":500,"Message":"Business id Is not correct"})    
    serializer = BusinessImagesSerializer(business)
    return Response({"status":200,"Message":"Business Detail Profile","Business":serializer.data})


@api_view(['GET','POST','PUT'])
def appointment(request,format='json'):
    print("===========",request.data)
    serializer=AppointmentSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response({"status":200,"Message":"Business Appointment Created Successfully.","Detail":serializer.data})
    else:
        return Response({"status":500,"Message":serializer.errors,"Error":serializer.errors[list(serializer.errors)[0]][0]})