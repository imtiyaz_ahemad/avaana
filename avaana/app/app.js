'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ui.router',
  'ngMaterial',
  'ngAnimate',
  'ngAria',
  'slick',
  'mdPickers',
  'ui.bootstrap',
  'ngImgCrop',
  'avaanaController',
  "avaanaService",
  "ui.calendar"
])
.constant('apiUrl', 'http://172.16.16.177:9000/')
.config(['$stateProvider','$urlRouterProvider','$httpProvider','$locationProvider', function($stateProvider,$urlRouterProvider,$httpProvider,$locationProvider) {
// $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
// $httpProvider.defaults.headers.common['Access-Control-Allow-Origin'] = '*'

  $stateProvider
    
    .state('app', {
      url: "/app",
      views:{
           "main":{
             templateUrl: "static/templates/app.html",
             controller: "appCtrl"    
       }
    }
      
    })


    .state('app.home', {
      url: "/home",
      views:{
           "content":{
             templateUrl: "static/templates/home.html",
             controller: "homeCtrl"    
       },
       "searchBar":{
            templateUrl: "static/templates/searchBar.html",
       }
    }
      
    })

    .state('app.provider', {
      url: "/provider",
      views:{
           "content":{
             templateUrl: "static/templates/provider.html",
             controller: "providerCtrl"    
       },
       "searchBar":{
            templateUrl: "static/templates/searchBar.html",
       }
    }
      
    })


    .state('app.search_result', {
      url: "/search_result",
      views:{
           "content":{

              templateUrl: "static/templates/search_result.html",
           //  templateUrl: "static/components/search/searchResult.html",

             controller:"searchResultCtrl"

          }
    } 

    })


    .state('app.login', {
      url: "/login",
      views:{
           "content":{
             templateUrl: "static/templates/login.html",
              controller: "loginCtrl"   
       }
    } 
    })


    .state('app.listyourbusiness', {
      url: "/listyourbusiness",
      views:{
           "content":{
             templateUrl: "static/templates/listyourbusiness.html",
             controller: "listBusinessCtrl",
       }
    }
      
    })


    .state('app.register', {
      url: "/register",
      views:{
           "content":{
             templateUrl: "static/templates/register.html",
             controller: "registerCtrl"    
        },
        
      }
   
    })
    .state('app.providerDashboard', {
      url: "/providerDashboard",
      views:{
           "content":{
             templateUrl: "static/templates/avaprovider.html",
             controller: "providerDashboardCtrl"    
        }
      }
   
    })

    .state('app.payment', {
      url: "/payment",
      views:{
           "content":{
             templateUrl: "static/templates/payment.html",
             controller: "paymentCtrl"    
        },
        "searchBar":{
            templateUrl: "static/templates/searchBar.html",
      }
    }
   
    });

  $urlRouterProvider.otherwise("/app/home");
  // $locationProvider.html5Mode(true);
}]);
