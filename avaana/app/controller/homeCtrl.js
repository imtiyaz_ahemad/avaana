angular.module("avaanaController")

.controller("homeCtrl",function($scope,$window,$state,paymentService){


  $scope.payment=function(amount){
    paymentService.global.amount=amount;
    $state.go('app.payment')
   }


   $scope.home = {
       showSponser:false,
       service:"Services or Business",
       location:"location1",
       showSponserSlide:3
   }

   $scope.home.toggleSponser = function(){
      $scope.home.showSponser=!$scope.home.showSponser;
   }
   
   if($window.innerWidth>1200 && $scope.home.showSponserSlide!=3){
          $scope.home.showSponserSlide=3;
        }
        else if($window.innerWidth<780 && $scope.home.showSponserSlide!=1){
          $scope.home.showSponserSlide=1;
        }
        else if($window.innerWidth>780  && $window.innerWidth<1200 && $scope.home.showSponserSlide!=2){
          $scope.home.showSponserSlide=2;
        }



    angular.element($window).bind('resize', function(){


        if($window.innerWidth>1200 && $scope.home.showSponserSlide!=3 && $scope.home.showSponser){
          $scope.home.showSponser=false;
          $scope.$digest();
          $scope.home.showSponserSlide=3;
          $scope.home.showSponser=true;
          $scope.$digest(); 
        }
        else if($window.innerWidth<780 && $scope.home.showSponserSlide!=1 && $scope.home.showSponser){
          $scope.home.showSponser=false;
          $scope.$digest();
          $scope.home.showSponserSlide=1;
          $scope.home.showSponser=true;
          $scope.$digest();
        }
        else if($window.innerWidth>780  && $window.innerWidth<1200 && $scope.home.showSponserSlide!=2 && $scope.home.showSponser){
          $scope.home.showSponser=false;
          $scope.$digest();
          $scope.home.showSponserSlide=2;
          $scope.home.showSponser=true;
          $scope.$digest();
        }
        

         // scope.width = $window.innerWidth;

         
         // scope.$digest();
       });

    $scope.openProviderWindow = function(){
      $state.go('app.provider');
    }


   // $scope.$watch('home.screenWidth',function(newVal,oldVal){
   //       console.log("ScreenWidth: "+newVal);
   // })

})