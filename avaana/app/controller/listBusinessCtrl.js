angular.module("avaanaController")

.controller("listBusinessCtrl",function($scope,$window,$timeout,$uibModal,$timeout,$filter,listBusinessService){

	var numRegex = /^\d+$/;
	var emailRegex=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	var websiteRegex = new RegExp("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?")
   

  $scope.business = {

  	   workDays:[  {day:'Monday',isChecked:false,startTime:'none' ,endTime:'none',priceErr:null,discountErr:null},
                   {day:'Tuesday',isChecked:false,startTime:'none',endTime:'none',priceErr:null,discountErr:null},
                   {day:'Wednesday',isChecked:false,startTime:'none', endTime:'none',priceErr:null,discountErr:null},
                   {day:'Thursday',isChecked:false,startTime:'none',endTime:'none',priceErr:null,discountErr:null},
                   {day:'Friday',isChecked:false,startTime:'none',endTime:'none',priceErr:null,discountErr:null},
                   {day:'Saturday',isChecked:false,startTime:'none',endTime:'none',priceErr:null,discountErr:null},
                   {day:'Sunday',isChecked:false,startTime:'none',endTime:'none',priceErr:null,discountErr:null}
                ],
  	   holidays:[],
       category:'Select Category',
       service:'Select Service',
       priceType:'All',
       discountType:'All',
       bookingType:'Other',
       priceArr:[],
       services:[],
       photoArr:[],
       logo:'static/image/account-icon.png',
       staffServiceTimeArr:[{service:'Please enter Service',time:'Please enter Time',serviceErr:null,timeErr:null}],
       staffHolidays:[],
       staff:[],
       status:{}
  }


  listBusinessService.get_user_business_api(localStorage.userId).then(function(success){
        console.log("business status: " + JSON.stringify(success));
       $scope.business.status = success.data.check_detail;
       console.log("status: "+JSON.stringify($scope.business.status))
       if(success.data.check_detail.business_id){
          listBusinessService.get_business_api(12).then(function(success){
          console.log("get business status: " + JSON.stringify(success));
          $scope.business.name = success.data.Business.name
          $scope.business.abn = success.data.Business.abn
          $scope.business.email = success.data.Business.name
          $scope.business.address = success.data.Business.address
          $scope.business.mobile = success.data.Business.mobile_no
          $scope.business.phone = success.data.Business.tel_no
          $scope.business.website = success.data.Business.website
          $scope.business.ogo_image = success.data.Business.logo_image
          $scope.business.workDays[0].startTime = success.data.Business.mon_start_at
          $scope.business.workDays[0].endTime = success.data.Business.mon_end_at


       })
        }
  },function(error){

  })


  /******************************* Contact Details end  ******************************************************/

  $scope.business.validateName = function(){
  	if($scope.business.name=='' || $scope.business.name==undefined || $scope.business.name==null){
  		$scope.business.nameErr="Please enter name";
  	}
  	else{
  	    $scope.business.nameErr=null;	
  	}
  }

  $scope.business.validateABN = function(){
  	if($scope.business.abn=='' || $scope.business.abn==undefined || $scope.business.abn==null){
  		$scope.business.abnErr="Please enter abn.";
  	}
  	else if(!numRegex.test($scope.business.abn)){
        $scope.business.abnErr="Please enter only numbers.";
  	}
  	else if($scope.business.abn.length!=11){
  	    $scope.business.abnErr="length of ABN should be 11.";	
  	}
  	else{
  	    $scope.business.abnErr=null;	
  	}
  }

  $scope.business.validateAddress = function(){
  	if($scope.business.address=='' || $scope.business.address==undefined || $scope.business.address==null){
  		$scope.business.addressErr="Please enter address.";
  	}
  	else{
  	    $scope.business.addressErr=null;	
  	}
  }

  $scope.business.validateEmail = function(){
   if($scope.business.email=='' || $scope.business.email==undefined || $scope.business.email==null){
   	   $scope.business.emailErr="Please enter email.";
   }
   else if(!emailRegex.test($scope.business.email)){
   	   $scope.business.emailErr="Please enter valid email.";
   }
   else{
   	   $scope.business.emailErr=null;
   }
 

  }

  $scope.business.validateMobile = function(){


  	 if( $scope.business.mobile!='' && $scope.business.mobile!=undefined && $scope.business.mobile!=null){

         //console.log( $scope.business.mobile.length);
         if(!numRegex.test( $scope.business.mobile )){
           $scope.business.mobileErr="Please enter only numbers."
         }
         else if( $scope.business.mobile.length!=10){
           $scope.business.mobileErr="Length of mobile no should be 10."
         }
         else{
           $scope.business.mobileErr=null; 	
         }
  	 }
  	 else{
  	 	$scope.business.mobileErr=null;
  	 }
  }

  $scope.business.validatePhone = function(){

  	if( $scope.business.phone!='' && $scope.business.phone!=undefined && $scope.business.phone!=null){

         //console.log( $scope.business.mobile.length);
         if(!numRegex.test( $scope.business.phone )){
           $scope.business.phoneErr="Please enter only numbers."
         }
         else if( $scope.business.phone.length!=9){
           $scope.business.phoneErr="Length of phone no should be 9."
         }
         else{
           $scope.business.phoneErr=null; 	
         }
  	 }
  	 else{
  	 	$scope.business.phoneErr=null;
  	 }
  }


  $scope.business.validateWebsite = function(){

  	if( $scope.business.website!='' && $scope.business.website!=undefined && $scope.business.website!=null){

         if(!websiteRegex.test( $scope.business.website )){
           $scope.business.websiteErr="Please enter valid website."
         }
         else{
           $scope.business.websiteErr=null; 	
         }
  	 }
  	 else{
  	 	$scope.business.websiteErr=null;
  	 }
  }


  $scope.business.saveContactDetails = function(){
     $scope.business.validateName();
     $scope.business.validateABN();
     $scope.business.validateAddress();
     $scope.business.validateEmail();
     $scope.business.validateMobile();
     $scope.business.validatePhone();
     $scope.business.validateWebsite();

     if($scope.business.nameErr==null && $scope.business.abnErr==null && 
        $scope.business.addressErr==null && $scope.business.emailErr==null &&
        $scope.business.mobileErr==null && $scope.business.phoneErr==null &&
        $scope.business.websiteErr==null
      ){
        data = {
    name: $scope.business.name,
    abn: $scope.business.abn,
    address: $scope.business.address,
    email: $scope.business.email,
    user: 3,
    website: $scope.business.website,
    mobile_no: $scope.business.mobile,
    tel_no: $scope.business.phone,
    hour_check:true,
    logo_image:null
  }


  console.log("data: "+JSON.stringify(data));
    listBusinessService.createbusiness_api(data).then(function(success){
          console.log('success:'+JSON.stringify(success));
          if (success.data.status == 500){
            $scope.business.apiError = success.data.Error;
            
            console.log('Error:::::::::::::'+JSON.stringify(Object.keys(success.data.Message)));
            error_key=Object.keys(success.data.Message);
            if(error_key[0]==='abn'){
              $scope.register.abnErr="Business with this ABN already exists.";
            

            }else if(error_key[0]==='email'){
              $scope.register.emailErr="Email Exist Already";

            }else{
              $scope.register.addressErr=success.data.Error;
            }
          }else{
            $scope.business.apiError = null;
            $scope.business.status.hour_check=true;
          //$state.go("app.listyourbusiness");
          }
           },function(error){
            console.log('Error:'+JSON.stringify(error));
           }); 
     }




  }


  /******************************* Contact Details end  ******************************************************/

  /************************************** About your Business Starts **************************************************/

  $scope.business.saveBusiness = function(){
    var nonSelected = 0;
    var errorInHours = "false";
     
     for(i=0;i< $scope.business.workDays.length;i++){

      if($scope.business.workDays[i].isChecked){
        $scope.business.changeStartDate( $scope.business.workDays[i] );
        $scope.business.changeEndDate( $scope.business.workDays[i] );
      }
      else{
        nonSelected++;
      }
             

       if($scope.business.workDays[i].startTimeErr!=null || $scope.business.workDays[i].endTimeErr!=null ||
          $scope.business.workDays[i].rangeErr!=null)
         {   
           errorInHours = "true";
        
         }
     }


     $scope.business.changeHolidays();

     if($scope.business.services.length==0){
        $scope.business.servicesErr = "Please enter at least one service";
     }
     else{
        $scope.business.servicesErr=null;
     }


     $scope.business.validateProfile();


     if($scope.business.photoArr.length==0){
         $scope.business.photoErr = "Please select at least one photo."
     }
     else{
         $scope.business.photoErr=null;
     }

     if($scope.business.logo=='static/image/account-icon.png'){
       $scope.business.logoErr = "Please select logo."
     }
     else{
       $scope.business.logoErr = null;
     }

     $scope.business.validateCancellation();
     $scope.business.validateLeadTime();
     $scope.business.validateFeatureTime();



     if(nonSelected==$scope.business.workDays.length){
        $scope.business.operationHourErr = "Please select hour of operation"
        return;
     }
     else{
        $scope.business.operationHourErr = null;
     }

     if(errorInHours=="true"){
       return;
     }

     if($scope.business.holidays.length==0 && $scope.business.holidayErr!=null){
        return;       
     }

     if( $scope.business.holidays.length==0){
      $scope.business.holidayErr="Please enter at least one holiday.";
      return;
     }
     else if($scope.business.holidays.length!=0){
       $scope.business.holidayErr=null;
     }

     if( $scope.business.servicesErr!=null){
       return;
     }

     if($scope.business.profileErr !=null){
       return;
     }

     if($scope.business.photoErr!=null){
       return;
     }

     if($scope.business.logoErr!=null){
      return;
     }

     if($scope.business.cancellationErr!=null || $scope.business.leadTimeErr!=null ||
$scope.business.featureTimeErr!=null){
        return;
     }

     /*************** write service code here ****************************/

        data = {
    name: $scope.business.name,
    abn: $scope.business.abn,
    address: $scope.business.address,
    email: $scope.business.email,
    user: 3,
    // mon_start_at : $scope.business.phone
    logo_image:$scope.business.logo,
    cancelation_policy:$scope.business.cancellation,
    max_lead_time:$scope.business.leadTime,
    max_num:$scope.business.featureTime,
    practitioner_check:true,
    holidays:$scope.business.holidays,
    services:$scope.business.services

  }

  

  if($scope.business.workDays[0].isChecked){
    data.mon_start_at=$filter('date')($scope.business.workDays[0].startTime, 'hh:mm');
    data.mon_end_at=$filter('date')($scope.business.workDays[0].endTime, 'hh:mm');
  }

  if($scope.business.workDays[1].isChecked){
    data.tue_start_at=$filter('date')($scope.business.workDays[1].startTime, 'hh:mm');
    data.tue_end_at=$filter('date')($scope.business.workDays[1].endTime, 'hh:mm');
  }

  if($scope.business.workDays[2].isChecked){
    data.wed_start_at=$filter('date')($scope.business.workDays[2].startTime, 'hh:mm');
    data.wed_end_at=$filter('date')($scope.business.workDays[2].endTime, 'hh:mm');
  }

  if($scope.business.workDays[3].isChecked){
    data.thr_start_at=$filter('date')($scope.business.workDays[3].startTime, 'hh:mm');
    data.thr_end_at=$filter('date')($scope.business.workDays[3].endTime, 'hh:mm');
  }

  if($scope.business.workDays[4].isChecked){
    data.fri_start_at=$filter('date')($scope.business.workDays[4].startTime, 'hh:mm');
    data.fri_end_at=$filter('date')($scope.business.workDays[4].endTime, 'hh:mm');
  }

  if($scope.business.workDays[5].isChecked){
    data.sat_start_at=$filter('date')($scope.business.workDays[5].startTime, 'hh:mm');
    data.sat_end_at=$filter('date')($scope.business.workDays[5].endTime, 'hh:mm');
  }

  if($scope.business.workDays[6].isChecked){
    data.sun_start_at=$filter('date')($scope.business.workDays[6].startTime, 'hh:mm');
    data.sun_end_at=$filter('date')($scope.business.workDays[6].endTime, 'hh:mm');
  }


  console.log("data: "+JSON.stringify(data));
    listBusinessService.update_business_api(13,data).then(function(success){
          console.log('success:'+JSON.stringify(success));
          if (success.data.status == 500){
            $scope.business.apiError = success.data.Error;
            
            console.log('Error:::::::::::::'+JSON.stringify(Object.keys(success.data.Message)));
            error_key=Object.keys(success.data.Message);
            if(error_key[0]==='abn'){
              $scope.register.abnErr="Business with this ABN already exists.";
            

            }else if(error_key[0]==='email'){
              $scope.register.emailErr="Email Exist Already";

            }else{
              $scope.register.addressErr=success.data.Error;
            }
          }else{
            $scope.business.apiError = null;
          $scope.business.status.practitioner_check=true;
          alert("Yupiee");
          }
           },function(error){
            console.log('Error:'+JSON.stringify(error));
           }); 
     

     
     
     
  }



  $scope.business.changeStartDate = function(days){

    
    if(days.startTime=='none' || days.startTime==null ){
      $scope.business.priceDiscountChange();
      days.startTimeErr = "Please enter start Time."
    }
  	else if(days.endTime!='none' && days.endTime!=null  && days.startTime!=null && days.startTime!='none'){
      days.startTimeErr = null;
  		if(days.startTime > days.endTime){
  			days.rangeErr = "Start time should be less than end time."
        $scope.business.priceDiscountChange();
  		}
  		else{
  		   days.rangeErr=null;
         $scope.business.priceTypeErr=null;
         $scope.business.discountTypeErr=null;	
  		}
  	}
    else{
      days.startTimeErr = null; 
    }
  }

  $scope.business.changeEndDate = function(days){

    if(days.endTime=='none' || days.endTime==null){
      days.endTimeErr = "Please enter end Time."
      $scope.business.priceDiscountChange();
    }
  	else if(days.startTime!='none' && days.startTime!=null && days.endTime!=null && days.endTime!='none'){

      days.endTimeErr=null;
  		if(days.startTime > days.endTime){
      
  			days.rangeErr = "End time should be greater than start time."
        $scope.business.priceDiscountChange();
  		}
  		else{
  		   days.rangeErr=null;
         $scope.business.priceTypeErr=null;	
         $scope.business.discountTypeErr=null;
  		}
  	}
    else{
      days.endTimeErr=null;
    }
  }

  $scope.business.changeHolidays = function(){
  	if( $scope.business.holiday == '' || $scope.business.holiday == undefined || $scope.business.holiday == null){
  		$scope.business.holidayErr = "Please enter holiday."
  	}
  	else{
  	    $scope.business.holidayErr = null;	
    }
  }

  $scope.business.addHolidays = function(){
  	
  	$scope.business.changeHolidays();
  
    $timeout(function(){
		if($scope.business.holidayErr==null){
         var present=false;

        for(i=0;i< $scope.business.holidays.length;i++){
          if( $scope.business.holidays[i].getDate() == $scope.business.holiday.getDate() && 
              $scope.business.holidays[i].getMonth() == $scope.business.holiday.getMonth()
            ){
               present=true;
               break;
          }
        }

        if(!present){
          $scope.business.holidays.push($filter('date')($scope.business.holiday, 'yyyy-MM-dd'));
          $scope.business.holiday=null;
          $scope.$apply();
        }
        //if($scope.business.holidays.indexOf( $scope.business.holiday)==-1)
				    // $scope.business.holidays.push($scope.business.holiday);
			}
        
    })
  	

  	
  }

  $scope.business.removeHoliday = function(index){
    $scope.business.holidays.splice(index,1);
  }


/**************************** Service  Section **********************************************/

  $scope.business.changeCategory = function(category){
     if(category=="Select Category"){
        $scope.business.categoryErr = "Please select category.";
     }
     else{
       $scope.business.categoryErr = null;
     }
  }

 $scope.business.changeService = function(service){
     if(service=="Select Service"){
        $scope.business.serviceErr = "Please select service.";
     }
     else{
       $scope.business.serviceErr = null;
     }
  }


$scope.business.validateLengthService = function(){
  if( $scope.business.lengthService == "" || 
      $scope.business.lengthService == undefined || $scope.business.lengthService == null){
        $scope.business.lengthServiceErr="Please enter length of business.";
  }
  else if(!numRegex.test( $scope.business.lengthService )){
        $scope.business.lengthServiceErr="Please enter only numbers(Non decimal)."; 
  }
  else if($scope.business.lengthService % 5 !=0){
        $scope.business.lengthServiceErr="Length should be in 5 minute increments"; 
  }
  else{
        $scope.business.lengthServiceErr=null; 
  }

}

$scope.business.priceTypeChange = function(){
  if( $scope.business.priceType=="All" ){
     $scope.business.priceTypeErr=null;
  }
  else if($scope.business.priceType=="Seperate"){
    $scope.business.priceErr=null;
    var notSelectedDays = 0;
    //$scope.business.priceArr=[];

     for(i=0;i< $scope.business.workDays.length;i++){
       if(!$scope.business.workDays[i].isChecked || 
           $scope.business.workDays[i].startTime=='none' ||
           $scope.business.workDays[i].startTime==null || 
           $scope.business.workDays[i].endTime=='none'|| 
           $scope.business.workDays[i].endTime==null  ||  
           $scope.business.workDays[i].rangeErr!=null){
            notSelectedDays++;
            continue;

       }
       else{
      /*   $scope.business.priceArr.push({
            day:$scope.business.workDays[i].day
         })*/
       }


     }

     if(notSelectedDays == $scope.business.workDays.length){
        $scope.business.priceTypeErr = "Please first select hours of operation."
     }
     else{
        $scope.business.priceTypeErr = null;
     }
  }

  
}

$scope.business.validateAllPrice = function(){
  if($scope.business.price=="" || $scope.business.price==undefined || $scope.business.price==null){
     $scope.business.priceErr="Please enter price."
  }
  else if(!numRegex.test($scope.business.price)){
     $scope.business.priceErr="Please enter only numbers." 
  }
  else {
    $scope.business.priceErr=null; 
  }
}

$scope.business.validatePrice = function(price){

  if(!price.isChecked){
    return;
  }

  if(price.price=="" || price.price==undefined || price.price==null){
     price.priceErr="Please enter price."
  }
  else if(!numRegex.test( price.price)){
     price.priceErr="Please enter only numbers." 
  }
  else {
    price.priceErr=null; 
  }
}

$scope.business.discountTypeChange = function(){
  if( $scope.business.discountType=="All" ){
     $scope.business.discountTypeErr=null;
  }
  else if($scope.business.discountType=="Seperate"){

    $scope.business.discountErr=null;
    var notSelectedDays = 0;
    //$scope.business.priceArr=[];

     for(i=0;i< $scope.business.workDays.length;i++){
       if(!$scope.business.workDays[i].isChecked || 
           $scope.business.workDays[i].startTime=='none' ||
           $scope.business.workDays[i].startTime==null || 
           $scope.business.workDays[i].endTime=='none' ||
           $scope.business.workDays[i].endTime==null || 
           $scope.business.workDays[i].rangeErr!=null){
            notSelectedDays++;
            continue;

       }
       else{

       }


     }

     if(notSelectedDays == $scope.business.workDays.length){
        $scope.business.discountTypeErr = "Please first select hours of operation."
     }
     else{
        $scope.business.discountTypeErr = null;
     }
  }

  
}

$scope.business.priceDiscountChange = function(days)
{

  if(days!=undefined && !days.isChecked ){
    days.rangeErr=null;
    days.startTimeErr=null;
    days.endTimeErr=null;
  }
 

  $scope.business.discountTypeChange();
  $scope.business.priceTypeChange();

}


$scope.business.validateAllDiscount = function(){
  if($scope.business.discountPrice!="" && $scope.business.discountPrice!=undefined && $scope.business.discountPrice!=null){
  
  if(!numRegex.test( $scope.business.discountPrice )){
     $scope.business.discountErr="Please enter only numbers." 
  }
  //else if( $scope.business.priceType=="All" )
  else {

     if($scope.business.priceType=="All"){ 

        if(  parseInt($scope.business.price)-parseInt($scope.business.discountPrice) < 0 ){
            $scope.business.discountErr="Discount cannot be more than price."
        }
        else{
            $scope.business.discountErr=null; 
        }
      }
      else if($scope.business.priceType=="Seperate" ){
      
           $scope.business.discountErr=null; 
            for(i=0;i< $scope.business.workDays.length;i++){

                if($scope.business.workDays[i].isChecked){
                  
                 if( ( parseInt( $scope.business.workDays[i].price ) - parseInt($scope.business.discountPrice) ) < 0 ) {
                     $scope.business.discountErr = "Discount cannot be more than price."
                     return;
                 }

               }  
            }    
    }
        

    
  }
 }
 else{
    $scope.business.discountErr=null;
 }
}

$scope.business.validateDiscount = function(price){

  if(!price.isChecked){
    return;
  }

  if( price.discount!="" || price.discount!=undefined || price.discount!=null){
  
    if(!numRegex.test( price.discount)){
     price.discountErr="Please enter only numbers." 
    }
    else {

    if($scope.business.priceType=="All"){ 

        if(  parseInt($scope.business.price)-parseInt(price.discount) < 0 ){
            price.discountErr="Discount cannot be more than price."
        }
        else{
            price.discountErr=null; 
        }
      }
      else if($scope.business.priceType=="Seperate" ){
      
           price.discountErr=null; 
            for(i=0;i< $scope.business.workDays.length;i++){

                if($scope.business.workDays[i].isChecked && ($scope.business.workDays[i].day == price.day) ){
                  
                 if( ( parseInt( $scope.business.workDays[i].price ) - parseInt(price.discount) ) < 0 ) {
                     price.discountErr = "Discount cannot be more than price."
                     return;
                 }

               }  
            }    
    }


    //price.discountErr=null; 
  }
 }
 else{
   price.discountErr=null;
 }
}


$scope.business.addService = function(){
  $scope.business.changeCategory( $scope.business.category );
  $scope.business.changeService( $scope.business.service );
  $scope.business.validateLengthService();
  $scope.business.priceTypeChange();
  if( $scope.business.priceType=="All" ){
      $scope.business.validateAllPrice();
  }
  else if( $scope.business.priceType=="Seperate" )  {
      for(i=0;i< $scope.business.workDays.length;i++){
        $scope.business.validatePrice($scope.business.workDays[i]);
      }
  }
  if( $scope.business.discountType=="All" ){
      $scope.business.validateAllDiscount();
  }
  else if( $scope.business.discountType=="Seperate" )  {
      for(i=0;i< $scope.business.workDays.length;i++){
        $scope.business.validateDiscount($scope.business.workDays[i]);
      }
  }

  if($scope.business.categoryErr !=null || $scope.business.serviceErr!=null || $scope.business.lengthServiceErr!=null
     || $scope.business.priceTypeErr!=null || $scope.business.discountTypeErr!=null){
    return;
  }
  else if($scope.business.priceType=="All" && $scope.business.priceErr!=null){
    return;
  }
  else if( $scope.business.discountType=="All" && $scope.business.discountErr!=null){
    return;
  }

  if($scope.business.priceType=="Seperate"){
    for(i=0;i< $scope.business.workDays.length;i++){
      if( $scope.business.workDays[i].priceErr!=null){
        return;
      }
    }
  }

  if( $scope.business.discountType=="Seperate" ){
    for(i=0;i< $scope.business.workDays.length;i++){
      if($scope.business.workDays[i].discountErr!=null){
        return;
      }
    }
  }
  
  
    var data = {
                 'category': $scope.business.category,
                 'service': $scope.business.service,
                 'length': $scope.business.lengthService,
                 "bookingType": $scope.business.bookingType

    }

    if($scope.business.priceType=="All"){
       data.price = $scope.business.price
    }
    else if($scope.business.priceType=="Seperate"){
      data.price = [];
       for (i=0;i< $scope.business.workDays.length;i++){
           if($scope.business.workDays[i].isChecked){
              data.price.push({
                                "day": $scope.business.workDays[i].day,
                                "price": $scope.business.workDays[i].price,

              })
           }
       } 
    }

    if($scope.business.discountType=="All"){
       data.discount = $scope.business.discountPrice
    }
    else if($scope.business.discountType=="Seperate"){
      data.discount = [];
       for (i=0;i< $scope.business.workDays.length;i++){
           if($scope.business.workDays[i].isChecked){
              data.discount.push({
                                "day": $scope.business.workDays[i].day,
                                "price": $scope.business.workDays[i].discount      
              })
           }
       } 
    }

    $scope.business.services.push(data);

    $scope.business.serviceErr=null;

    $scope.business.category="Select Category";
    $scope.business.service="Select Service";
    $scope.business.lengthService=null;
    $scope.business.bookingType="Other";

    if($scope.business.priceType=="All"){
       $scope.business.price=null
    }
    else if($scope.business.priceType=="Seperate"){
       for (i=0;i< $scope.business.workDays.length;i++){
           if($scope.business.workDays[i].isChecked){
                  $scope.business.workDays[i].price=null;
           }
       } 
    }

     if($scope.business.discountType=="All"){
       $scope.business.discountPrice=null;
    }
    else if($scope.business.discountType=="Seperate"){
     for (i=0;i< $scope.business.workDays.length;i++){
           if($scope.business.workDays[i].isChecked){
              
                  $scope.business.workDays[i].discount=null;     
              
           }
       } 
    }


    console.log("services: "+JSON.stringify($scope.business.services));
}



$scope.business.checkObject = function(val){
   if(typeof val =="object")
   return true;
   else
   return false; 
}

$scope.business.validateProfile = function(){
   if($scope.business.profile=="" || $scope.business.profile==undefined || $scope.business.profile==null){
     $scope.business.profileErr="Please enter profile."
   }
   else{
     $scope.business.profileErr=null; 
   }
}

$scope.business.validateCancellation = function(){
  if($scope.business.cancellation=="" || $scope.business.cancellation==undefined || $scope.business.cancellation==null){
     $scope.business.cancellationErr="Please enter cancellation Policy."
   }
   else if(!numRegex.test($scope.business.cancellation)){
     $scope.business.cancellationErr="Please enter valid cancellation Policy."
   }
   else{
     $scope.business.cancellationErr=null; 
   }
}

$scope.business.validateLeadTime = function(){
  if($scope.business.leadTime =="" || $scope.business.leadTime==undefined || $scope.business.leadTime==null){
     $scope.business.leadTimeErr="Please enter lead time."
   }
   else if(!numRegex.test($scope.business.leadTime)){
     $scope.business.leadTimeErr="Please enter valid lead time."
   }
   else{
     $scope.business.leadTimeErr=null; 
   }
}

$scope.business.validateFeatureTime = function(){
  if($scope.business.featureTime=="" || $scope.business.featureTime==undefined || $scope.business.featureTime==null){
     $scope.business.featureTimeErr="Please enter feature Time."
   }
   else if(!numRegex.test($scope.business.featureTime)){
     $scope.business.featureTimeErr="Please enter valid feature Time."
   }
   else{
     $scope.business.featureTimeErr=null; 
   }
}

$scope.business.openPhotoModal = function(type){
    $scope.photoModel = $uibModal.open({
      templateUrl: 'static/templates/photoModal.html',
      controller: 'listBusinessCtrl',
      scope: $scope
      
    });

    $scope.type=type;

 
 $scope.photoModel.rendered.then(function(success){

    $scope.myImage='';
    $scope.myCroppedImage='';

    

    var handleFileSelect=function(evt) {
      var file=evt.currentTarget.files[0];
      var reader = new FileReader();
      reader.onload = function (evt) {
        $scope.$apply(function($scope){
          $scope.myImage=evt.target.result;
        });
      };
      reader.readAsDataURL(file);
    };
    
    angular.element(document.querySelector('#businessPhoto')).on('change',handleFileSelect);  
    
    

 },function(error){
   
   

 })

}

$scope.business.choosePhoto = function(){
 

  if( $scope.myImage==null || $scope.myImage==undefined || $scope.myImage==""){
      $scope.cropperImageErr = "Please select image."
  }
  else{
    $scope.cropperImageErr=null;
    if($scope.type=="photo"){
        $scope.$emit("photoChoosen",{img: $scope.myImage})
    }
    else if($scope.type=="logo"){
       $scope.$emit("logoChoosen",{img: $scope.myImage}) 
    }
    $scope.$close();  
  }


  
}


$scope.$on("photoChoosen",function(evt,data){
  $scope.business.photoArr.push(data.img);
  $scope.business.photoErr = null;
})

$scope.$on("logoChoosen",function(evt,data){
  $scope.business.logo=data.img;
  $scope.business.logoErr = null;
})




/************************************** About your Business Ends **************************************************/

/************************************** Staff Section  Start ******************************************************/
$scope.business.validateStaffName = function(){
  if( $scope.business.staffName == "" || $scope.business.staffName == undefined || $scope.business.staffName == null){
     $scope.business.staffNameErr = "Please enter staff name." 
  } 
  else{
    $scope.business.staffNameErr = null;
  } 
}

$scope.business.validateStaffProfile = function(){
  if( $scope.business.staffProfile == "" || $scope.business.staffProfile == undefined || $scope.business.staffProfile == null){
     $scope.business.staffProfileErr = "Please enter staff profile." 
  } 
  else{
    $scope.business.staffProfileErr = null;
  } 
}


// $scope.validateServiceTime = function(){
//       if(serviceTime.service =="Please enter Service"){
//         serviceTime.serviceErr = "Enter service to add more entries."
//       }
//       else{
//        serviceTime.serviceErr =null; 
//       }
//       if(serviceTime.time == "Please enter Time"){
//         serviceTime.timeErr = "Enter time to add more entries." 
//       }
//       else{
//        serviceTime.timeErr = null; 
//       }
// }


$scope.business.appendServiceTime = function(serviceTime){
    if(serviceTime.service =="Please enter Service"){
        serviceTime.serviceErr = "Enter service to add more entries."
      }
      else{
       serviceTime.serviceErr =null; 
      }
      if(serviceTime.time == "Please enter Time"){
        serviceTime.timeErr = "Enter time to add more entries." 
      }
      else{
       serviceTime.timeErr = null; 
      }
  

  if(serviceTime.serviceErr==null && serviceTime.timeErr==null){
    $scope.business.staffServiceTimeArr.push({service:'Please enter Service',time:'Please enter Time',serviceErr:null,timeErr:null});
    $scope.business.serviceTimeArrayErr=null;
  }
}

$scope.business.deleteServiceTime = function(index){
 $scope.business.staffServiceTimeArr.splice(index,1);
}

$scope.business.validateStaffUsername = function(){
  if( $scope.business.staffUserName == "" || $scope.business.staffUserName == undefined || $scope.business.staffUserName == null){
     $scope.business.staffUserNameErr = "Please enter staff user name." 
  } 
  else{
    $scope.business.staffUserNameErr = null;
  } 
}

$scope.business.validateStaffEmail = function(){
   if($scope.business.staffEmail=='' || $scope.business.staffEmail==undefined || $scope.business.staffEmail==null){
       $scope.business.staffEmailErr="Please enter email.";
   }
   else if(!emailRegex.test($scope.business.staffEmail)){
       $scope.business.staffEmailErr="Please enter valid email.";
   }
   else{
       $scope.business.staffEmailErr=null;
   }
 

  }

  $scope.business.validateStaffPassword = function(){
  if( $scope.business.staffPassword == "" || $scope.business.staffPassword == undefined || $scope.business.staffPassword == null){
     $scope.business.staffPasswordErr = "Please enter password." 
  } 
  else{
    $scope.business.staffPasswordErr = null;
  } 
}

$scope.business.changeStaffHolidays = function(){
    if( $scope.business.staffHoliday == '' || $scope.business.staffHoliday == undefined || $scope.business.staffHoliday == null){
      $scope.business.staffHolidayErr = "Please enter holiday."
    }
    else{
        $scope.business.staffHolidayErr = null;  
    }
  }

  $scope.business.addStaffHolidays = function(){
    
    $scope.business.changeStaffHolidays();
  
    $timeout(function(){
    if($scope.business.staffHolidayErr==null){
         var present=false;

        for(i=0;i< $scope.business.staffHolidays.length;i++){
          if( $scope.business.staffHolidays[i].getDate() == $scope.business.staffHoliday.getDate() && 
              $scope.business.staffHolidays[i].getMonth() == $scope.business.staffHoliday.getMonth()
            ){
               present=true;
               break;
          }
        }

        if(!present){
          $scope.business.staffHolidays.push($filter('date')($scope.business.staffHoliday, 'yyyy-MM-dd'));
          $scope.business.staffHolidayArrayErr=null;
          $scope.$apply();
        }
        
      }
        
    })
    

    
  }

  $scope.business.removeStaffHoliday = function(index){
     $scope.business.staffHolidays.splice(index,1);
  }


  $scope.business.addPractioners = function(){
     $scope.business.validateStaffName();
     $scope.business.validateStaffProfile();
     $scope.business.validateStaffEmail();
     $scope.business.validateStaffPassword();
     $scope.business.validateStaffUsername();

     if($scope.business.staffServiceTimeArr.length==1){
      $scope.business.serviceTimeArrayErr = "Please enter atleast one service time"; 
     }
     else{
      $scope.business.serviceTimeArrayErr = null;
     }

     if($scope.business.staffHolidays.length==0){
      $scope.business.staffHolidayArrayErr = "Please enter atleast one holiday"; 
     }
     else{
      $scope.business.staffHolidayArrayErr = null;
     }


     if($scope.business.staffNameErr==null && $scope.business.staffProfileErr==null &&
        $scope.serviceTimeArrayErr==null && $scope.business.staffPasswordErr==null &&
        $scope.business.staffEmailErr==null && $scope.business.staffUserNameErr==null &&
        $scope.business.staffHolidayArrayErr == null 
        ){
          
          var data = {
                            name:$scope.business.staffName.trim(),
                            profile:$scope.business.staffProfile.trim(),
                            username:$scope.business.staffUserName.trim(),
                            email:$scope.business.staffEmail.trim(),
                            password:$scope.business.staffPassword.trim(),
                            business:12,
                            serviceTime:[],
                            holidays:$scope.business.staffHolidays
               
                          }
          // $scope.business.staff.push)

          for(i=0;i<$scope.business.staffServiceTimeArr.length;i++){
             if( $scope.business.staffServiceTimeArr[i].service!='Please enter Service' && 
              $scope.business.staffServiceTimeArr[i].time!='Please enter Time'){
               data.serviceTime.push({
                      service:$scope.business.staffServiceTimeArr[i].service,
                      time:$scope.business.staffServiceTimeArr[i].time
                   })
             }
          }
          console.log("staffData: "+JSON.stringify(data));
    listBusinessService.createpractitioner_api(data).then(function(success){
          console.log('success:'+JSON.stringify(success));
          if (success.data.status == 500){
            $scope.business.apiError = success.data.Error;
            
            console.log('Error:::::::::::::'+JSON.stringify(Object.keys(success.data.Message)));
            error_key=Object.keys(success.data.Message);
            if(error_key[0]==='abn'){
              $scope.register.abnErr="Business with this ABN already exists.";
            

            }else if(error_key[0]==='email'){
              $scope.register.emailErr="Email Exist Already";

            }else{
              $scope.register.addressErr=success.data.Error;
            }
          }else{
            $scope.business.apiError = null;
          $scope.business.status.practitioner_check=true;
          alert("Yupiee");
          }
           },function(error){
            console.log('Error:'+JSON.stringify(error));
           }); 

     }
  }



    
    

/****************************** Staff Section end ********************************************/



$scope.list = function(){
if ($scope.business.nameErr==null &&  $scope.business.abnErr==null && $scope.business.addressErr==null
 && $scope.business.emailErr==null && $scope.business.mobileErr==null && $scope.business.phoneErr==null 
 && $scope.business.websiteErr==null ){
  data = {
    name: $scope.business.name,
    abn: $scope.business.abn,
    address: $scope.business.address,
    email: $scope.business.email,
    user: 1,
    website: $scope.business.website,
    mobile_no: $scope.business.mobile,
    tel_no: $scope.business.phone
  }
  console.log("data: "+JSON.stringify(data));
    listBusinessService.createbusiness_api(data).then(function(success){
          console.log('success:'+JSON.stringify(success));
          if (success.data.status == 500){
            $scope.business.apiError = success.data.Error;
            
            console.log('Error:::::::::::::'+JSON.stringify(Object.keys(success.data.Message)));
            error_key=Object.keys(success.data.Message);
            if(error_key[0]==='abn'){
              $scope.register.abnErr="Business with this ABN already exists.";
            

            }else if(error_key[0]==='email'){
              $scope.register.emailErr="Email Exist Already";

            }else{
              $scope.register.addressErr=success.data.Error;
            }
          }else{
            $scope.business.apiError = null;
          $state.go("app.home");
          alert("Yupiee");
          }
           },function(error){
            console.log('Error:'+JSON.stringify(error));
           });
  }

}




})