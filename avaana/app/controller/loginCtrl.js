angular.module("avaanaController")

.controller("loginCtrl",function($scope,$window,$state,$http,authService){

$scope.register=function(){
    $state.go('register')
   }

   
   $scope.obj={};


   var emailRegex=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
   var authWindow;
    var googleapi = {
        authorize: function(options) {
            //var deferred = $.Deferred();
            //Build the OAuth consent page URL
            var authUrl = 'https://accounts.google.com/o/oauth2/v2/auth?' + $.param({
                client_id: options.client_id,
                redirect_uri: options.redirect_uri,
                response_type: 'token',
                scope: options.scope,
               // nonce:"DgkRrHXmyu3KLd0KDdfq"
            });
            window.location.href = authUrl;
        }
    }

    var facebookapi = {
      authorize: function(options) {
            //var deferred = $.Deferred();
            //Build the OAuth consent page URL
            var authUrl = 'https://www.facebook.com/dialog/oauth?' + $.param({
                client_id: options.client_id,
                redirect_uri: options.redirect_uri,
                response_type: 'token',
                scope: options.scope,
               // nonce:"DgkRrHXmyu3KLd0KDdfq"
            });

            window.location.href = authUrl;
        }
    }


   $scope.obj.login = function(){
       if($scope.obj.username=="" || $scope.obj.username==undefined || $scope.obj.username==null){
          $scope.obj.usernameErr = "Please enter email";
        }
        else if(!emailRegex.test($scope.obj.username)){
          $scope.obj.usernameErr = "Please enter valid email";
        }
        else{
          $scope.obj.usernameErr=null;
        }

        if($scope.obj.password=="" || $scope.obj.password==undefined || $scope.obj.password==null){
          $scope.obj.passwordErr = "Please enter password";
        }
        else{
          $scope.obj.passwordErr =null;
        }

        if($scope.obj.usernameErr==null && $scope.obj.passwordErr==null){
          localStorage.loginType = "normal";
          data={
               email:$scope.obj.username,
               password:$scope.obj.password
          }
           console.log("data: "+JSON.stringify(data));
           authService.login_api(data).then(function(success){
            if (success.data.status == 200 ){
              console.log('success login:'+JSON.stringify(success));
              localStorage.userId = success.data.user.id
              $scope.$emit("loggedIn");
              $state.go("app.home");
              $scope.obj.loginErr=null;
            }else if(success.data.status == 500){
              console.log('Error::::'+JSON.stringify(success));
              $scope.obj.loginErr=success.data.Message;
            }
           },function(error){
            console.log('Error::::'+JSON.stringify(error));
             $scope.obj.loginErr=error.data.Message;
           });
        }
   }




   $scope.obj.googlePlusLogin = function(){
         localStorage.loginType = "google";
         //$scope.$emit("loggedIn");

         googleapi.authorize({
            client_id: '319578356248-99pomcnc52vhr77aemkt38uv8i40462v.apps.googleusercontent.com',
            client_secret: 'mJjCQil6EK1EG8AYD4BLd342',
            redirect_uri: 'http://localhost:8000/static/redirect.html',                            
            scope:'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile'                
        })
   }

   $scope.obj.facebookLogin = function(){
       localStorage.loginType = "facebook";
         facebookapi.authorize({
            client_id: '1074049999369075',
            redirect_uri: 'http://localhost:8000/static/redirect.html',                         
            scope:'public_profile email'
        })
   }
})

          // $http.post('http://localhost:8000/a_login/', data,{ 'headers':{'content-type':'application/json'}}).then(function(result) {

          //        if(result.data.status==200){
          //             $scope.$emit("loggedIn");
          //             $state.go("app.home");
          //             $scope.obj.loginErr=null;
          //        }
          //        else if(result.data.status==500){
          //            $scope.obj.loginErr=result.data.Message;
          //        }    
          //  });