angular.module("avaanaController")

.controller("paymentCtrl",function($scope,paymentService,$state){


	$scope.payment={
     amount : paymentService.global.amount

  };

	var emailRegex=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    var phoneRegex=/^(\+\d{1,3}[- ]?)?\d{10}$/
   

   $scope.payment.placeOrder = function(){
      $scope.payment.validateFirstName();
      $scope.payment.validateEmail();
      $scope.payment.validPhone();
      $scope.payment.validCardNo();
      $scope.payment.validateSequirityCode();
      $scope.payment.validateCardHolder();
      $scope.payment.validateExpiryDate();
      $scope.payment.validatePromoCardName();

      if($scope.fullnameErr==null && $scope.emailErr==null && $scope.phoneErr==null && $scope.cardnoErr==null &&
      	$scope.cardHolderErr==null && $scope.expiryDateErr==null && $scope.promocardNameErr==null && $scope.sequirityCodeErr==null){

         data={
          full_name:$scope.payment.fullName,
          email:$scope.payment.email,
          phone:$scope.payment.phone,
          card_number:$scope.payment.cardno,
          cvc:$scope.payment.sequirityCode,
          cardHolder:$scope.payment.cardHolder,
          exp_month:$scope.payment.expiryDate.getMonth()+1,
           exp_year:$scope.payment.expiryDate.getFullYear(),
          amount:parseInt(paymentService.global.amount)*100,
          currency:"usd",
          promocardName:$scope.payment.promocardName
      }
          console.log("data: "+JSON.stringify(data));
          paymentService.payment_order(data).then(function(success){
          console.log('success:'+JSON.stringify(success));
          $state.go("app.home");
           },function(error){
            console.log('Error:'+JSON.stringify(error));
           });
      }


   }

   $scope.payment.validateFirstName = function(){
   	  if($scope.payment.fullName=="" || $scope.payment.fullName==undefined || $scope.payment.fullName==null){
      	$scope.fullnameErr="Please enter first name.";
      }
      else{
      	$scope.fullnameErr=null;
      }
   }

   $scope.payment.validateEmail = function(){

   	 if($scope.payment.email=="" || $scope.payment.email==undefined || $scope.payment.email==null){
       $scope.emailErr="Please enter email.";	
      }
      else if(!emailRegex.test($scope.payment.email)){
      	$scope.emailErr="Please enter correct email."
      }
      else{
        $scope.emailErr=null;	
      }
   } 

   $scope.payment.validPhone = function(){
   	if($scope.payment.phone=="" || $scope.payment.phone==undefined || $scope.payment.phone==null){
       $scope.phoneErr="Please enter Phone no.";	
      }
      else if(!phoneRegex.test($scope.payment.phone)){
      	$scope.phoneErr="Please enter correct phone no."
      }
      else{
        $scope.phoneErr=null;	
      }

   }

   $scope.payment.validCardNo = function(){
     if($scope.payment.cardno=="" || $scope.payment.cardno==undefined || $scope.payment.cardno==null){
       $scope.cardnoErr="Please enter card no.";	
      }
      else if(!Stripe.card.validateCardNumber($scope.payment.cardno)){
      	$scope.cardnoErr="Please enter valid cardno."
      }
      else{
        $scope.cardnoErr=null;	
      }


   } 
   $scope.payment.validateSequirityCode = function(){
     if($scope.payment.sequirityCode=="" || $scope.payment.sequirityCode==undefined || $scope.payment.sequirityCode==null){
       $scope.sequirityCodeErr="Please enter sequrity code.";	
      }
      else if(!Stripe.card.validateCVC($scope.payment.sequirityCode)){
      	$scope.sequirityCodeErr="Please enter valid sequrity code."
      }
      else{
        $scope.sequirityCodeErr=null;	
      }


   }  

   $scope.payment.validateCardHolder = function(){
   	if($scope.payment.cardHolder=="" || $scope.payment.cardHolder==undefined || $scope.payment.cardHolder==null){
       $scope.cardHolderErr="Please enter card holder's name";	
      }
      else{
        $scope.cardHolderErr=null;	
      }
   }

   $scope.payment.validateExpiryDate = function(){
     if($scope.payment.expiryDate=="" || $scope.payment.expiryDate==undefined || $scope.payment.expiryDate==null){
       $scope.expiryDateErr="Please enter expiry.";	
      }
      else if(!Stripe.card.validateExpiry($scope.payment.expiryDate.getMonth()+1,$scope.payment.expiryDate.getFullYear())){
      	$scope.expiryDateErr="Please enter valid expiry date."
      }
      else{
        $scope.expiryDateErr=null;	
      }
  }

  $scope.payment.validatePromoCardName = function(){
    if($scope.payment.promocardName=="" || $scope.payment.promocardName==undefined || $scope.payment.promocardName==null){
       $scope.promocardNameErr="Please enter card holder's name";	
      }
      else{
        $scope.promocardNameErr=null;	
      }
 }




})