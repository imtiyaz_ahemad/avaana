angular.module("avaanaController")

.controller("providerCtrl",function($scope,$window){
   
   $scope.provider={
       serviceOpen:false,
       staffOpen:false,
       socialFieldOpen:false,
       sayingOpen:false

   }

   $scope.provider.serviceToggle = function(){
      $scope.provider.serviceOpen=!$scope.provider.serviceOpen;
   }

   $scope.provider.staffToggle = function(){
    $scope.provider.staffOpen=!$scope.provider.staffOpen;
   }

   $scope.provider.socialToggle = function(){
    $scope.provider.socialFieldOpen=!$scope.provider.socialFieldOpen;
   }

   $scope.provider.sayingToggle = function(){
    $scope.provider.sayingOpen=!$scope.provider.sayingOpen;
   }



})