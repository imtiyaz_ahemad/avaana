angular.module("avaanaController")

.controller("registerCtrl",function($scope,$window,authService,$state,$http){

  $scope.login=function(){
    $state.go('login')
   }

  $scope.register={};
   var emailRegex=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    var phoneRegex=/^(\+\d{1,3}[- ]?)?\d{10}$/


   $scope.register.signup = function(){
        if($scope.register.first_name=="" || $scope.register.first_name==undefined || $scope.register.first_name==null){
          $scope.register.firstnameErr = "Please enter your first name";
        }
        else{
          $scope.register.firstnameErr =null;
        }
        if($scope.register.last_name=="" || $scope.register.last_name==undefined || $scope.register.last_name==null){
          $scope.register.lastnameErr = "Please enter your last name"; 
        }
        else{
          $scope.register.lastnameErr =null;
        }
        if($scope.register.username=="" || $scope.register.username==undefined || $scope.register.username==null){
          $scope.register.usernameErr = "Please enter email";
        }
        else if(!emailRegex.test($scope.register.username)){
          $scope.register.usernameErr = "Please enter valid email";
        }
        else{
          $scope.register.usernameErr=null;
        }
        if($scope.register.password=="" || $scope.register.password==undefined || $scope.register.password==null){
          $scope.register.passwordErr = "Please enter password";
        }
        else{
          $scope.register.passwordErr =null;
        }
        if($scope.register.phone != undefined || $scope.register.phone!=null) {
          if(!phoneRegex.test($scope.register.phone)){
            $scope.register.phoneErr = "Please enter valid Phone Number";
          }
          else{
           $scope.register.phoneErr =null;  
          } 
        }
        else{
          $scope.register.phoneErr =null;
        }
        if($scope.register.usernameErr==null && $scope.register.passwordErr==null && $scope.register.firstnameErr ==null && $scope.register.lastnameErr ==null &&$scope.register.phoneErr ==null){
          data={
            first_name:$scope.register.first_name,
            last_name:$scope.register.last_name,
            email:$scope.register.username,
            password:$scope.register.password,
            phone_no:$scope.register.phone,
            provider:localStorage.loginType,
            u_id:$scope.register.u_id
          }
          console.log("data:::::::: "+JSON.stringify(data));
          authService.sign_up_api(data).then(function(success){
          if(success.data.status==200){
              console.log('success register:'+JSON.stringify(success));
              localStorage.userId = success.data.User_Detail.id;
              //alert("userId: "+localStorage.userId)
              $state.go("app.provider");
            $scope.register.usernameErr=null;
          }else if(success.data.status==500){
            console.log('Error:::::::::::::'+JSON.stringify(Object.keys(success.data.Message)));
            error_key=Object.keys(success.data.Message);
            if(error_key[0]==='email'){
              $scope.register.usernameErr="Email address already exits.";
            }else if(error_key[0]==='password'){
              $scope.register.passwordErr="Password too Short";
            }else{
              $scope.register.phoneErr=success.data.Error;
            }
          }
            
          },function(error){
            console.log('Error:::'+JSON.stringify(error));
          });
          }
        }

   if(localStorage.loginType == "google"){
	   authService.getGoogleData().then(function(success){
	      console.log("Success: "+JSON.stringify(success));
          $scope.register.username = success.data.emails[0].value;
          $scope.register.first_name = success.data.name.givenName;
          $scope.register.last_name = success.data.name.familyName;
          $scope.register.u_id=success.data.id;
	   },function(error){
	      console.log("error: "+JSON.stringify(error));
	   })
  }
  else if(localStorage.loginType == "facebook"){
     authService.getFacebookData().then(function(success){
	      console.log("Success: "+JSON.stringify(success));
          $scope.register.username = success.data.email;
          $scope.register.first_name = success.data.first_name;
          $scope.register.last_name = success.data.last_name;
          $scope.register.u_id=success.data.id;
	   },function(error){
	      console.log("error: "+JSON.stringify(error));
	   })
  }	



})



