angular.module("avaanaService",[])

.service("authService",function($q,$http){
   this.getGoogleData = function(){
   	  var defer = $q.defer();
      $http.get('https://www.googleapis.com/plus/v1/people/me',{ 'headers':{'Authorization':'Bearer '+localStorage.googleAccessToken}})
        .then(function(succ){
              defer.resolve(succ)                    
        },function(error){
              defer.reject(error);
        })
      return defer.promise;
   }
   this.getFacebookData = function(){
   	  var defer = $q.defer();
      $http.get('https://graph.facebook.com/v2.5/me?access_token='+localStorage.facebookAccessToken+'&fields=name,email,gender,birthday,first_name,last_name,picture')
        .then(function(succ){
              defer.resolve(succ)                    
        },function(error){
              defer.reject(error);
        })
      return defer.promise;
   }



   this.login_api = function(data){
        var defer = $q.defer();
      $http.post("http://localhost:8000/a_login/",data)        
      .then(function(success){
         defer.resolve(success); 
      },function(error){
         defer.reject(error);
      })
      return defer.promise;  
   }



   this.sign_up_api = function(data){
        var defer = $q.defer();
      $http.post("http://localhost:8000/a_signup/",data)        
      .then(function(success){
         defer.resolve(success); 
      },function(error){
         defer.reject(error);
      })
      return defer.promise;  
   }
})









