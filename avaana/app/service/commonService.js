angular.module("avaanaService")

.service("apiService", function($q, $http, apiUrl) {


    this.getData = function(endpoint1,methodType) {
    	//alert(methodType)
//alert(apiUrl + endpoint)
        var defer = $q.defer();
           var  httpOptions = {
                url: apiUrl + endpoint1,
               
                method: 'get',
                Accept: 'application/json',
                headers: {
                    'Content-Type': 'application/json'
                }
            };
           // alert(apiUrl + endpoint)
        $http(httpOptions).then(function(response) {
            defer.resolve(response);
        }, function(err) {
            defer.reject(err);
        });
        return defer.promise;




    }



})