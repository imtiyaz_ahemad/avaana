angular.module("avaanaService")

.service("listBusinessService",function($q,$http){



  this.get_user_business_api = function(id){
        var defer = $q.defer();
      $http.get("http://localhost:8000/a_get_user_business/"+id)        
      .then(function(success){
         defer.resolve(success); 
      },function(error){
         defer.reject(error);
      })
      return defer.promise;  
   }

this.createbusiness_api = function(data){
        var defer = $q.defer();
      $http.post("http://localhost:8000/a_createbusiness/",data)        
      .then(function(success){
         defer.resolve(success); 
      },function(error){
         defer.reject(error);
      })
      return defer.promise;  
   }


   this.createbusinessservice_api = function(data){
        var defer = $q.defer();
      $http.post("http://localhost:8000/a_createbusinessservice/",data)        
      .then(function(success){
         defer.resolve(success); 
      },function(error){
         defer.reject(error);
      })
      return defer.promise;  
   }


   this.createbusinessholiday_api = function(data){
        var defer = $q.defer();
      $http.post("http://localhost:8000/a_createbusinessholiday/",data)        
      .then(function(success){
         defer.resolve(success); 
      },function(error){
         defer.reject(error);
      })
      return defer.promise;  
   }


    this.update_business_api = function(id,data){
        var defer = $q.defer();
      $http.post("http://localhost:8000/a_update_business/"+id+"/",data)        
      .then(function(success){
         defer.resolve(success); 
      },function(error){
         defer.reject(error);
      })
      return defer.promise;  
   }

  this.createpractitioner_api = function(data){
        var defer = $q.defer();
      $http.post("http://localhost:8000/a_createpractitioner/",data)        
      .then(function(success){
         defer.resolve(success); 
      },function(error){
         defer.reject(error);
      })
      return defer.promise;  
   }

  this.get_business_api = function(id){
        var defer = $q.defer();
      $http.get("http://localhost:8000/a_get_business/"+id)        
      .then(function(success){
         defer.resolve(success); 
      },function(error){
         defer.reject(error);
      })
      return defer.promise;  
   }

});