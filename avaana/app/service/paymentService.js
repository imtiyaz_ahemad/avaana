angular.module("avaanaService")

.service("paymentService",function($q,$http){

   this.global={};

   this.payment_order = function(data){
        var defer = $q.defer();

      $http.post("http://localhost:8000/a_place_order/",data)        
      .then(function(success){
         defer.resolve(success); 
      },function(error){
         defer.reject(error);
      })
      return defer.promise;  
   }
})