from settings import *

# export DJANGO_SETTINGS_MODULE=avaana.dev_settings
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR + 'localdatabase',
    }
}