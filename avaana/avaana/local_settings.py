import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DEBUG = True

####For Postgres Setiing would be
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'avaana_web',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}